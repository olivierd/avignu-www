Ce dépôt contient les fichiers ainsi que les thèmes utilisés sur notre site.

* [Dotclear](https://fr.dotclear.org/)
* [DokuWiki](https://www.dokuwiki.org/fr:dokuwiki)

Les bibliothèques suivantes :
* [chota](https://jenil.github.io/chota/)
* [Leaflet](https://leafletjs.com/)

On utilise également les icônes [Bootstrap Icons](https://github.com/twbs/icons).

Pour convertir les images SVG → PNG, il faut installer sur les systèmes Debian, Linux Mint (ou Ubuntu) le paquet `librsvg2-bin`.

    apt install librsvg2-bin

Ci-dessous un example pour obtenir une image PNG « transparente » de 48 pixels :

    rsvg-convert -a -f png -w 48 -o ~/tips.png lightbulb.svg
