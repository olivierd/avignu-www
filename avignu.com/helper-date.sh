#!/usr/bin/sh -eu

# Update date and time for meta tag
#

#file=$(git diff --cached --name-status | grep 'index.html' \
#       | awk '{printf("%s", $2)}')

sed -i -r "s/name=\"date\" content=\"[0-9-]+T[0-9:+]+/name=\"date\" content=\"$(date --iso-8601=seconds)/" index.html
