Les fichiers `helper-*` et `Makefile` sont destinés à une utilisation « local ».

* `helper-local.sh` modifie le chemin vers la bibliothèque (locale) Leaflet suite au commit [5da42c84](https://framagit.org/olivierd/avignu-www/-/commit/5da42c84b231c240509c190d6f0fd3f06614309d)
* `helper-remote.sh` ré-ajuste le chemin de la bibliothèque présente sur le serveur
* `helper-date.sh` change la date et l'heure de la balise meta dans le fichier `index.html`

Le `Makefile` facilite ces modifications :

* après `git clone` → `make local`
* si le contenu du fichier `index.html` n'a pas été modifié → `make remote`
* si le contenu du fichier `index.html` a été modifié → `make pre-commit`

La cible **pre-commit** a pour pré requis **remote** et **adjust-date**.

La cible **adjust-date** met uniquement à jour la date et l'heure dans le fichier `index.html`.

Le hook **pre-commit** laisse toujours les fichiers dans un état *staged* (modifiés). C'est pourquoi il n'est pas utilisé.
