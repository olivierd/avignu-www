#!/usr/bin/sh -eu

#
# Change local path of Leaflet files (.js and .css).
# It is intended for remote repository
#

### Don't change anything below
PRGNAME="${0##*/}"

usage() {
    cat <<EOF >&2
usage: ${PRGNAME} option

available options:
   -h     show this help
   -f     file to modify
EOF
    exit 0
}

to_remote_path() {
    if [ -e ${1} ]; then
        if [ "${1}" = "index.html" ]; then
            sed -i -r "s|=\"../dotclear|=\"./dotclear/themes|g" ${1}
        fi
    fi

    exit 0
}

opts=$(getopt hf: ${*})
if [ ${?} -ne 0 ]; then
    usage
fi
set -- ${opts}

if [ ${#} -eq 1 ]; then
    usage
else
    while true; do
        case "${1}" in
            -h)
                usage ;
                shift
                ;;
            -f)
                to_remote_path ${2}
                shift
                ;;
            --)
                shift ; break
                ;;
        esac
    done
fi
echo ${#}
