# Fichiers modifiés concernant DokuWiki

- `main.php`, `tpl_footer.php` et `tpl_header.php` → `lib/tpl/dokuwiki/`
- `userstyle.css` → `conf/`
- `Edit.php` → `inc/Menu/Item/`

## Les images

### Comment sont-elles obtenues ?

Elles proviennent tous d'une archive [Bootstrap Icons](https://github.com/twbs/icons).

Celles que nous utilisons se trouvent dans le fichier `img-list.txt`. Pour en obtenir une copie :

```
while read f; do
    cp le/chemin/vers/archive/bootstrap-icons/$f .
done < img-list.txt
```

Elles doivent être transformées, afin d'avoir les mêmes caractéristiques que celles utilisées dans DokuWiki.

C'est le script `icons-dokuwiki.sh` qui s'en charge (consultez-le pour installer les pré requis), et crée l'arborescence.

```
sh icons-dokuwiki.sh
```

Le contenu du dossier `images/menu/` → `lib/images/menu/`.

Le fichier `admin.svg` remplace ceux, se trouvant dans les sous répertoires de `lib/plugins/`.

## Plugin wrap

Les images situées dans `plugins/wrap/images/` → `plugins/wrap/images/note/32/`.

Le fichier `style.less` → `lib/plugins/wrap/`.
