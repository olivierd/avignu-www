#!/usr/bin/sh

# SPDX-License-Identifier: Unlicense
#
# Resize (increase) SVG image from bootstrap-icons tarball in order
# to be reused in DokuWiki.
#
# On Debian (or Ubuntu-based distros) install
#    librsvg2-bin, scour
#

if [ ! -d "images/menu/" ]; then
    mkdir -p images/menu
fi

# pencil.svg → 01-edit_pencil.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      pencil.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/01-edit_pencil.svg \
               --enable-id-stripping 1> /dev/null

# pencil.svg → 02-new-document.svg
## /!\ Don't forget to change inc/Menu/Item/Edit.php
cp images/menu/01-edit_pencil.svg \
   images/menu/02-new-document.svg

rm ~/tmp_file.svg

# file-earmark-text.svg → 04-show_file-document.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      file-earmark-text.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/04-show_file-document.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# file-earmark-code.svg → 05-source_file-xml.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      file-earmark-code.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/05-source_file-xml.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# clock-history.svg → 07-revisions_history.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      clock-history.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/07-revisions_history.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# link-45deg.svg → 08-backlink_link-variant.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      link-45deg.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/08-backlink_link-variant.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# arrow-up.svg → 10-top_arrow-up.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      arrow-up.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/10-top_arrow-up.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# arrow-left.svg → 12-back_arrow-left.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      arrow-left.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/12-back_arrow-left.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# person-vcard.svg → account-card-details.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      person-vcard.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/account-card-details.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# person-plus.svg → account-plus.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      person-plus.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/account-plus.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# person.svg → login.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      login.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/login.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# power.svg → logout.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      power.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/logout.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# gear-fill.svg → settings.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      gear-fill.svg ;
$(which scour) -i ~/tmp_file.svg -o images/menu/settings.svg \
               --enable-id-stripping 1> /dev/null
# Same image, but for config plugin
if [ ! -d "plugins/config" ]; then
    mkdir -p plugins/config/
fi
cp images/menu/settings.svg plugins/config/admin.svg

rm ~/tmp_file.svg

# key.svg → admin.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      key.svg ;
if [ ! -d "plugins/acl" ]; then
    mkdir -p plugins/acl/
fi
$(which scour) -i ~/tmp_file.svg -o plugins/acl/admin.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# journal-text.svg → admin.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      journal-text.svg ;
if [ ! -d "plugins/logviewer" ]; then
    mkdir -p plugins/logviewer/
fi
$(which scour) -i ~/tmp_file.svg -o plugins/logviewer/admin.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# palette2.svg → admin.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      palette2.svg ;
if [ ! -d "plugins/styling" ]; then
    mkdir -p plugins/styling/
fi
$(which scour) -i ~/tmp_file.svg -o plugins/styling/admin.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# people.svg → admin.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      people.svg ;
if [ ! -d "plugins/usermanager" ]; then
    mkdir -p plugins/usermanager/
fi
$(which scour) -i ~/tmp_file.svg -o plugins/usermanager/admin.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg

# send.svg → admin.svg
$(which rsvg-convert) -a -w 24 -h 24 -f svg -o ~/tmp_file.svg \
                      send.svg ;
if [ ! -d "plugins/popularity" ]; then
    mkdir -p plugins/popularity/
fi
$(which scour) -i ~/tmp_file.svg -o plugins/popularity/admin.svg \
               --enable-id-stripping 1> /dev/null

rm ~/tmp_file.svg
